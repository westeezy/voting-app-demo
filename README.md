# Voting App

This is a demo of a small voting app. The API and the client are totally separated currently and in their respective folders. Normally would be separate repos, but I wanted this to be easy for anyone to peek at.

It is currently deployed on now.sh for the client and heroku for the server. Both services are free tier so there may be some slowdowns, but it was sufficient to deploy there for a quick demo. See Possible Production Deployment below.

Production DB Creds will be sent along with the link to this repo so replace `server/src/config/production.js` with the file provided.

## Documentation

You can view their documentation here:

* [client](https://gitlab.com/westeezy/voting-app-demo/blob/master/client/README.md)
	* Has extensive section on TDT + Enhancements to be done 
* [server](https://gitlab.com/westeezy/voting-app-demo/blob/master/server/README.md)

## Quick Development Deploy

There are docker files in both client and server directories. Their respective readme's have nots on deployment but to have a quick and dirty docker dev deploy you can run the following: 

* `cd client`
	* `docker build -t voting-client .`
	* `docker run -p 3000:3000 voting-client` (not ideal for production)
* In another shell:
* `cd server`
	* `docker build -t voting-api .`
	* `docker run -p 3001:3001 voting-api` (uses hobby tier db so a bit slow on queries)

Note there is not a docker multicontainer setup as ideally you are deploying the Front End on a CDN but it is easy to change that around and serve the front end from the server later if needed.

## Possible Production Deployment

* Server can upload its docker images to ECS and deploy on Beanstalk or a custom setup EC2 cluster to allow for scaling. 

* DB can be setup on RDS Postgres.

* Client can be uploading to S3 and served from Cloudfront with desired edge nodes set.

## Why not build these together?

By seperating these out you have the freedom to deploy your frontend to a CDN like CloudFront and also more easily tune and use the API with other clients.
It's trivial to convert this to a single app if need be as one could just set up the server to mount a public directory which the client would build into.

## Demo of usage

It's mostly just a small bootstrap design. It is responsive but needs a decent amount of design tweaking to be pretty.

![alt tag](https://gitlab.com/westeezy/voting-app-demo/raw/master/demo.gif)
