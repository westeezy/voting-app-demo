exports.get = async (ctx, next) => {
  try {
    const userId = ctx.params.id || ctx.state.user.id;
    ctx.body = await ctx.Models.User.findById(userId, {
      include: [ctx.Models.Poll],
    });
  } catch (e) {
    ctx.status = 500;
    ctx.body = e.message;
  }
};
