exports.update = async (ctx, next) => {
  try {
    const id = ctx.params.id;
    const choice = await ctx.Models.Choice
      .findById(id)
      .then(choice => choice.increment('count'));

    ctx.body = choice;
  } catch (e) {
    ctx.status = 500;
    ctx.body = e.message;
  }
};
