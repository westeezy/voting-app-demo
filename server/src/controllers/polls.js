exports.get = async (ctx, next) => {
  try {
    const id = ctx.params.id;
    let polls;
    if (id) {
      polls = await ctx.Models.Poll.findById(id, {
        include: [ctx.Models.Choice],
      });
    } else {
      polls = await ctx.Models.Poll.findAll({
        attributes: ['id', 'question'],
      });
    }
    ctx.body = polls;
  } catch (e) {
    ctx.status = 500;
  }
};

exports.create = async (ctx, next) => {
  try {
    const { question, options } = ctx.request.body;
    const userId = ctx.state.user.id;

    if (!question || options.length === 0) {
      throw 'Please provide both a question and answer';
    }

    const poll = await ctx.Models.Poll.create({
      question,
      userId,
    });

    const choices = await ctx.Models.Choice.bulkCreate(
      options.map(opt => ({ text: opt, pollId: poll.id })),
    );
    ctx.body = poll;
  } catch (e) {
    ctx.status = 500;
    ctx.message = e.message;
  }
};
