const generateToken = require('../auth/generateToken');
const { test } = require('../auth/hash');

exports.login = async ctx => {
  try {
    const { username, password } = ctx.request.body;

    const user = await ctx.Models.User.findOne({
      where: { username },
    });

    if (user && test(password, user.password)) {
      ctx.body = {
        user,
        token: generateToken({ id: user.id }),
      };
    } else {
      ctx.status = 401;
    }
  } catch (e) {
    // don't want to leak any info so just always Unauth error of 401
    ctx.status = 401;
  }
};

exports.register = async ctx => {
  try {
    const { username, password } = ctx.request.body;
    const user = await ctx.Models.User.create({
      username,
      password,
    });
    ctx.body = user;
  } catch (e) {
    ctx.status = 500;
    ctx.body = 'Error creating user';
  }
};
