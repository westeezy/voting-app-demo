const jwt = require('jsonwebtoken');
const config = require('../config');

const generateToken = user => {
  const token = jwt.sign(
    {
      id: user.id,
      exp: Math.floor(new Date().getTime() / 1000) + config.auth.timeout,
    },
    config.auth.secret,
  );

  return token;
};

module.exports = generateToken;
