const config = require('../config');
const bcrypt = require('bcrypt');

// Hashes and generates salt
const encrypt = t => bcrypt.hashSync(t, config.auth.difficulty);

// Test for match
const test = (t, hash) => bcrypt.compareSync(t, hash);

module.exports = {
  encrypt,
  test,
};
