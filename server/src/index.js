const koa = require('koa');
const logger = require('koa-logger');
const jwt = require('koa-jwt');
const convert = require('koa-convert');
const cors = require('koa-cors');
const middleware = require('./middleware');
const limit = require('koa-limit');
const bodyParser = require('koa-bodyparser');

const config = require('./config');
const router = require('./routes');

const app = new koa();

// cors support
app.use(convert(cors()));

app.use(jwt({ secret: config.auth.secret }).unless({
    path: [/^\/login/, /^\/register/],
  }));

app.use(middleware.models);
app.use(convert(bodyParser()));

if (config.logging) {
  app.use(convert(logger()));
}

if (config.rateLimit) {
  // limit to 10 requests per second
  app.use(limit({
      limit: 10,
      interval: 1000 * 60,
    }));
}

app.use(router.routes()).use(router.allowedMethods());

module.exports = app;
