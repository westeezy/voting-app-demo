const config = require('../config');
const Sequelize = require('sequelize');

const { database, user, port, host } = config.db;

let sequelize;

if (process.env.DATABASE_URL) {
  sequelize = new Sequelize(process.env.DATABASE_URL, config.db.options);
} else {
  sequelize = new Sequelize(
    config.db.database,
    config.db.user,
    config.db.password,
    config.db.options,
  );
}

//const sequelize = new Sequelize(
//  `postgres://${user}@${host}:${port}/${database}`,
//  {
//    logging: config.log,
//  }
//);

module.exports = sequelize;
