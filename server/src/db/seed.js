const sequelize = require('./');
const User = require('../models/user');
const Poll = require('../models/poll');
const Choice = require('../models/choice');

// create some dummy data
//
async function create() {
  await User.sync({ force: true });
  const user = await User.create({
    username: 'westin',
    password: 'test',
  });

  await Poll.sync({ force: true });
  const poll = await Poll.create({
    question: 'Who is the best ever?',
    userId: user.id,
  });

  await Choice.sync({ force: true });
  const choices = await Choice.bulkCreate([
    {
      text: 'foo',
      pollId: poll.id,
    },
    {
      text: 'bar',
      pollId: poll.id,
    },
  ]);

  Choice.findById(1).then(choice => choice.increment('count'));
}

create();
