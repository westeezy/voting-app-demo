const models = require('../models');

// add models to ctx
module.exports = async (ctx, next) => {
  ctx.Models = models;
  await next();
};
