module.exports = {
  auth: {
    difficulty: 1,
  },
  db: {
    database: 'voting-app-test',
    user: 'westinw',
    password: '',
    options: {
      dialect: 'sqlite',
      storage: ':memory:',
      logging: false,
    },
  },
  logging: false,
  rateLimit: false,
};
