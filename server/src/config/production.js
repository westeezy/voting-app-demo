module.exports = {
  auth: {
    timeout: 7200,
    secret: 'super-secret-shared-secret',
  },
  db: {
    database: '',
    user: '',
    password: '',
    options: {
      port: 5432,
      host: 'ec2-54-225-240-168.compute-1.amazonaws.com',
      dialect: 'postgres',
      dialectOptions: {
        ssl: true,
      },
    },
  },
};
