module.exports = {
  auth: {
    timeout: 7200,
    secret: 'shared-secret',
    difficulty: 10,
  },
  logging: true,
  rateLimit: true,
  port: 3001,
};
