const Sequelize = require('sequelize');
const sequelize = require('../db');

const Poll = sequelize.define(
  'poll',
  {
    // Allow incrementing IDs rather than guids for "public" polls
    question: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    completed: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  },
  {
    timestamps: true,
  },
);

module.exports = Poll;
