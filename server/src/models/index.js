const User = require('./user');
const Poll = require('./poll');
const Choice = require('./choice');

module.exports = {
  User,
  Poll,
  Choice,
};

User.hasMany(Poll);
Poll.belongsTo(User);
Poll.hasMany(Choice);
