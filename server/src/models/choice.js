const Sequelize = require('sequelize');
const sequelize = require('../db');

const Choice = sequelize.define(
  'choice',
  {
    // Allow incrementing IDs rather than guids for "public" choices
    text: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    count: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
    },
  },
  {
    timestamps: true,
  },
);

module.exports = Choice;
