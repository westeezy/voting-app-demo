const Sequelize = require('sequelize');
const sequelize = require('../db');
const omit = require('lodash.omit');
const { encrypt } = require('../auth/hash');

const User = sequelize.define(
  'user',
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true,
      defaultValue: Sequelize.UUIDV4,
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
      set: function(p) {
        this.setDataValue('password', encrypt(p));
      },
    },
  },
  {
    timestamps: true,
    instanceMethods: {
      toJSON: function() {
        const blacklist = ['password', 'createdAt', 'updatedAt'];
        return omit(this.dataValues, blacklist);
      },
    },
  },
);

module.exports = User;
