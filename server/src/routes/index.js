const router = new require('koa-router')();
const AuthController = require('../controllers/auth');
const UserController = require('../controllers/user');
const PollsController = require('../controllers/polls');
const VoteController = require('../controllers/vote');

// Auth
router.post('/login', AuthController.login);
router.post('/register', AuthController.register);

// Users
router.get('/user/:id?', UserController.get);

// Polls
router
  .get('/poll/:id?', PollsController.get)
  .post('/poll', PollsController.create);

router.put('/vote/:id', VoteController.update);

module.exports = router;
