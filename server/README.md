# Voting App Server

Slim koa server for the voting app. Leverages a Postgres database and sequelize for an ORM. Keep in mind that requests are rate limited to 10 requests per second (at the ip level).

Currently deployed on a free version of heroku at [https://postgres-voting-app.herokuapp.com/](https://postgres-voting-app.herokuapp.com/). It may run a little on the slow slide but it's sufficient to demo.

## Enpoints
For all routes except auth supply a valid bearer token in the auth header as follows... `Authorization: Bearer <TOKEN>`

### Auth
 
 * POST /login
 	* Request Body: { username: '', password: '' }
 	* Response Body: { token } 	
 * POST /register
 	* Request Body: { username: '', password: '' }
 	* Response Body: 'Success'

### User

* GET /user /user/:id
	* Request Body: none
	* Reponse Body: {} 	

// Polls
### Poll

* GET /poll
	* Request Body: none 	
	* Response Body Example: `[ {"id":6,"question":"test"}, ... ]`
* GET /poll/:id
	* Request Body: none 
	* Response Body Example: 
	
		```{
		  "id": 6,
		  "question": "test",
		  "completed": false,
		  "createdAt": "2017-02-06T02:58:42.276Z",
		  "updatedAt": "2017-02-06T02:58:42.276Z",
		  "userId": "2163e487-59cb-abcd-efgh-1134436e2ec4",
		  "choices": [
		    {
		      "id": 9,
		      "text": "1",
		      "count": 15,
		      "createdAt": "2017-02-06T02:58:42.286Z",
		      "updatedAt": "2017-02-07T02:27:15.969Z",
		      "pollId": 6
		    },
		    ...
		  ]
		}
		```
* POST /poll
	* Request Body: { question: '', options: []} 
	* Reponse Body Example: 
	
		```
		{
		  "completed": false,
		  "id": 15,
		  "question": "reponse",
		  "userId": "2163e487-59cb-4bc4-8ace-1134436e2ec4",
		  "updatedAt": "2017-02-07T03:58:59.708Z",
		  "createdAt": "2017-02-07T03:58:59.708Z"
		}
		```

### Vote

* PUT /vote/:id (id marks the choices table id)
	* Request Body: None
	* Response Body Example: 
	
		```
		{
		  "id": 9,
		  "text": "1",
		  "count": 16,
		  "createdAt": "2017-02-06T02:58:42.286Z",
		  "updatedAt": "2017-02-07T04:00:07.949Z",
		  "pollId": 6
		}
		```


## Installation

I reccomend using [yarn](https://yarnpkg.com/) for npm packages.

 * `yarn`
 * Update `src/config/development` to suite your local setup
 * Create the postgres db which can be done via `createdb`
 * `yarn start`

 If you don't have postgres locally there is a heroku hobby tier instance set up in `config/production.js`.
 You can run `yarn start:herokudb` which will spawn up nodemon and connect to that db. For real prod uses it
 is reccomended to either use the Dockerfile and `yarn start:docker` or switch nodemon for pm2.

## Docker
There is a Dockerfile included so you can run:

* docker build -t voting-api .
* docker run -p 3001:3001 voting-api

Note this setup uses `babel-register` for startup transpiling to use async/await.
Node v7 (coming soon) gets that natively. For more details see the comment in `index.transpile.js`.


## Testing

Test were done in the most lightweight way possible at the gate for time efficiency.

The test suite leverages
  * [mocha](https://mochajs.org/) for the test framework
    * could later be substituted out for a TAP based runner especially AVA if we want concurrent tests.
    * for now this api is very small and most people are familiar with mocha already
  * [supertest](https://github.com/visionmedia/supertest) for integration tests
  * [assert](https://github.com/defunctzombie/commonjs-assert) for an assertion library
    * Could later be substituted out for something like expect, but that was not needed and less bloat is good.
  * [nyc](https://github.com/istanbuljs/nyc) for test coverage

### Test Directory

  * Mocha will run any `*.spec.js` file in the `/test` directory.
  * Integration tests are in their own subfolder for testing api usage
  * Unit tests are in their own folder for testing utility functions

### Running the tests (which includes coverage)

* `yarn test`

The coverage currently stands at....

* Statements   : 95.69% ( 111/116 )
* Branches     : 87.5% ( 49/56 )
* Functions    : 100% ( 28/28 )
* Lines        : 95.33% ( 102/107 )

## Linting

Testing out the awesome [prettier](https://github.com/jlongster/prettier) tool.
I've long waited for the day javascript had something similar to RustFmt/ElmFmt so hopefully this works out well.
I have configured a `yarn lint` script to auto update the file if your editor is not configured to run `prettier` on save.

## Deployment

You can use this handy Dockerfile to easily deploy an image to ECS and then go to either Beanstalk or EC2. Or whatever you want really, the world's your oyster so have fun!
