/*
 * This is a bit of a hack but node is getting async/await in v7.
 *
 * So without those features in core we have to transpile code + include a generator polyfill.
 * For a REAL app in production we would transpile as a build step, but as this is a smaller
 * app we can live with the startup time hit (which is minor for an app this size).
 *
 */
require('babel-register'); // transpile on startup
require('babel-polyfill'); // enable regenerator runtime needed for async/await
require('./');
