const assert = require('assert');
const request = require('supertest');
const app = require('../../src/index');
const helpers = require('../helpers');
const models = require('../../src/models');

describe('Users', function() {
  before(() => require('../../src/db').sync());

  describe('unauthorized', () => {
    after(() => models.User.destroy({ truncate: true }));

    it('should protect users route', () => {
      return request(app.listen()).get('/users').expect(401);
    });

    it('allow registration', () => {
      return request(app.listen())
        .post('/register')
        .send({ username: 'foo', password: 'bar' })
        .expect(200)
        .then(() => {
          const user = models.User.findOne({}).then(user => {
            assert.equal(user.username, 'foo');
            // password should be salted
            assert.notEqual(user.password, 'bar');
          });
        });
    });

    it('reject a bad registration body', () => {
      return request(app.listen()).post('/register').expect(500);
    });
  });

  describe('login', () => {
    before(() => helpers.createUser());
    after(() => models.User.destroy({ truncate: true }));

    it('should successfully login', () => {
      return request(app.listen())
        .post('/login')
        .send({ username: 'test', password: 'test' })
        .expect(200);
    });

    it('should reject a bad username/password', () => {
      return request(app.listen())
        .post('/login')
        .send({ username: 'foo', password: 'bar' })
        .expect(401);
    });

    it('should reject a bad request body', () => {
      return request(app.listen()).post('/login').send({}).expect(401);
    });
  });

  describe('api', () => {
    let testUser;

    before(() => {
      return helpers.createUser().then(user => {
        testUser = user;
      });
    });
    after(() => models.User.destroy({ truncate: true }));

    it('should retrieve my user profile', () => {
      return request(app.listen())
        .get('/user')
        .set('Authorization', `Bearer ${testUser.token}`)
        .expect(200, {
          id: testUser.id,
          username: testUser.username,
          polls: [],
        });
    });

    it('should retrieve a user by id', () => {
      return request(app.listen())
        .get(`/user/${testUser.id}`)
        .set('Authorization', `Bearer ${testUser.token}`)
        .expect(200, {
          id: testUser.id,
          username: testUser.username,
          polls: [],
        });
    });

    it('should 204 if id does not exist', () => {
      return request(app.listen())
        .get('/user/100')
        .set('Authorization', `Bearer ${testUser.token}`)
        .expect(204);
    });
  });
});
