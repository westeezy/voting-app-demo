const assert = require('assert');
const request = require('supertest');
const helpers = require('../helpers');
const app = require('../../src/index');
const models = require('../../src/models');

describe('Polls', function() {
  before(() => require('../../src/db').sync());

  describe('unauthorized', () => {
    it('should protect polls /GET route', () => {
      return request(app.listen()).get('/poll').expect(401);
    });

    it('should protect polls /GET by id route', () => {
      return request(app.listen()).get('/poll/1').expect(401);
    });

    it('should protect polls /POST route', () => {
      return request(app.listen()).post('/poll').expect(401);
    });
  });

  describe('authorized', () => {
    let testUser;
    before(() => {
      return Promise.all([
        models.Poll.bulkCreate([
          { question: 'question 1' },
          { question: 'question 2' },
        ]),
        helpers.createUser().then(user => testUser = user),
      ]);
    });

    after(() => {
      return Promise.all([
        models.Poll.destroy({ truncate: true }),
        models.User.destroy({ truncate: true }),
      ]);
    });

    it('should retrieve polls', () => {
      return request(app.listen())
        .get('/poll')
        .set('Authorization', `Bearer ${testUser.token}`)
        .expect(200)
        .then(response => {
          assert.equal(response.body.length, 2);
        });
    });

    it('should retrieve a poll by id', () => {
      return request(app.listen())
        .get('/poll/2')
        .set('Authorization', `Bearer ${testUser.token}`)
        .expect(200)
        .then(response => {
          assert.equal(response.body.id, 2);
        })
        .catch(e => console.error(e));
    });

    it('should send a 204 if poll id does not exist', () => {
      return request(app.listen())
        .get('/poll/100')
        .set('Authorization', `Bearer ${testUser.token}`)
        .expect(204);
    });

    it('should create a new poll', () => {
      return request(app.listen())
        .post('/poll')
        .set('Authorization', `Bearer ${testUser.token}`)
        .send({ question: 'foo', options: ['foo', 'bar', 'baz'] })
        .expect(200);
    });

    it('should fail poll creation with no body', () => {
      return request(app.listen())
        .post('/poll')
        .set('Authorization', `Bearer ${testUser.token}`)
        .expect(500);
    });
  });
});
