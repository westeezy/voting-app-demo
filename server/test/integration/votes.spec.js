const assert = require('assert');
const request = require('supertest');
const helpers = require('../helpers');
const app = require('../../src/index');
const models = require('../../src/models');

describe('Votes', function() {
  before(() => require('../../src/db').sync());

  describe('unauthorized', () => {
    it('should protect votes /POST route', () => {
      return request(app.listen()).post('/votes/1').expect(401);
    });
  });

  describe('authorize', () => {
    let testUser;
    before(() => {
      return Promise.all([
        models.Choice.create({ text: 'choice' }),
        helpers.createUser().then(user => testUser = user),
      ]);
    });

    after(() => {
      return Promise.all([
        models.Choice.destroy({ truncate: true }),
        models.User.destroy({ truncate: true }),
      ]);
    });

    it('should start a new choice at 0 votes', () => {
      return models.Choice.findOne().then(choice => {
        assert.equal(choice.count, 0);
      });
    });

    it('should increment a vote', () => {
      return request(app.listen())
        .put('/vote/1')
        .set('Authorization', `Bearer ${testUser.token}`)
        .expect(200);
    });

    it('should fail increment without a valid id', () => {
      return request(app.listen())
        .put('/vote/abc')
        .set('Authorization', `Bearer ${testUser.token}`)
        .expect(500);
    });
  });
});
