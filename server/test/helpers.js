const models = require('../src/models');
const generateToken = require('../src/auth/generateToken');

// create a user with the auth token
const createUser = (username = 'test', password = 'test') => {
  return models.User.create({ username, password }).then(user => {
    return Promise.resolve(Object.assign(user, {
        token: generateToken({ id: user.id }),
      }));
  });
};

module.exports = {
  createUser,
};
