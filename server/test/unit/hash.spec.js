const assert = require('assert');
const { encrypt, test } = require('../../src/auth/hash');

// see RFC for info on jwt formats
// https://tools.ietf.org/html/rfc7519

describe('hash', function() {
  it('should generate a salted value', () => {
    assert.notEqual(encrypt('test'), 'test');
  });

  it('should not match different values', () => {
    const hash = encrypt('asdf');
    assert.equal(test(hash, 'test'), false);
  });
});
