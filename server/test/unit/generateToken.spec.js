const assert = require('assert');
const jwt = require('jsonwebtoken');
const config = require('../../src/config');
const generateToken = require('../../src/auth/generateToken');

// see RFC for info on jwt formats
// https://tools.ietf.org/html/rfc7519

describe('generateToken', function() {
  it("should generate a valid jwt with user's id", () => {
    const id = '123-456';
    const token = generateToken({ id });
    const length = token.split('.').length;

    assert.equal(length, 3);
    assert.equal(jwt.verify(token, config.auth.secret).id, id);
  });
});
