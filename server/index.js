const app = require('./src');
const db = require('./src/db');
const config = require('./src/config');

const port = process.env.PORT || config.port || 3001;

// sync db and start server
db.sync().then(() => {
  console.info(`listening on ${port}`);
  app.listen(port);
});
