export const isAuthenticated = () => localStorage.getItem('token') !== null;
export const logout = () => localStorage.removeItem('token');
export const getToken = () => localStorage.getItem('token');
