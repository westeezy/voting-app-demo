import React from 'react';
import Header from './';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <Header
        isActive={path => 'http://localhost:3000/profile'.includes(path)}
      />,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
