import React, { PropTypes } from 'react';
import { Link } from 'react-router';

// TODO: support a mapping of any components
// for tab items

const Header = (
  {
    isActive = i => i,
  },
) => (
  <ul className="nav center">
    <li className="nav-item">
      <Link
        className={`nav-link ${isActive('/polls') ? 'active-nav' : ''}`}
        to="/polls"
      >
        Polls
      </Link>
    </li>
    <li className="nav-item">
      <Link
        className={`nav-link ${isActive('/profile') ? 'active-nav' : ''}`}
        to="/profile"
      >
        Profile
      </Link>
    </li>
    <li className="nav-item">
      <Link className="nav-link" to="/logout">Logout</Link>
    </li>
  </ul>
);

Header.propTypes = {
  isActive: PropTypes.func,
};

export default Header;
