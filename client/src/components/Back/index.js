import React from 'react';
import { browserHistory } from 'react-router';

const Back = (
  {
    children,
  },
) => (
  <small>
    <a className="back" onClick={browserHistory.goBack}>
      &lt; Back
    </a>
  </small>
);

export default Back;
