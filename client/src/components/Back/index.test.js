import React from 'react';
import Back from './';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <Back>
        Go Back
      </Back>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
