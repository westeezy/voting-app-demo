import React, { PropTypes } from 'react';

// Created this to easily swap to a nicer animated button
// if time permits

const Button = (
  {
    children,
    className = '',
    size = 'md',
    bStyle = 'primary',
    onClick = i => i,
  },
) => (
  <button
    className={`btn btn-${size} btn-outline-${bStyle} ${className}`}
    onClick={onClick}
  >
    {children}
  </button>
);

Button.propTypes = {
  className: PropTypes.string,
  size: PropTypes.string,
  bStyle: PropTypes.string,
  onClick: PropTypes.func,
};

export default Button;
