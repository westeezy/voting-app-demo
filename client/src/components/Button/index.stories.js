import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs, text } from '@kadira/storybook-addon-knobs';
import centered from '@kadira/react-storybook-decorator-centered';

import Button from './';

storiesOf('Button', module)
  .addDecorator(withKnobs)
  .addDecorator(centered)
  .add('Button', () => (
    <Button size={text('size', 'md')} bStyle={text('bStyle', 'primary')}>
      Click Me!
    </Button>
  ));
