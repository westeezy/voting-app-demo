import React, { PropTypes } from 'react';
import { BarChart, Bar, XAxis, Tooltip } from 'recharts';

const assignColors = data => data.map(d => ({
  ...d,
  fill: d.fill || '#' + Math.floor(Math.random() * 16777215).toString(16),
}));

const VoteChart = (
  {
    data = [],
    dataKey = 'value',
  },
) => (
  <BarChart width={200} height={300} data={assignColors(data)}>
    <XAxis dataKey="name" />
    <Tooltip />
    <Bar dataKey={dataKey} />
  </BarChart>
);

VoteChart.propTypes = {
  data: PropTypes.array,
  dataKey: PropTypes.string,
};

export default VoteChart;
