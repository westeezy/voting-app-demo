import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs, object } from '@kadira/storybook-addon-knobs';
import centered from '@kadira/react-storybook-decorator-centered';

import Chart from './';

const data = [
  { name: 'Page A', value: 4000, fill: 'green' },
  { name: 'Page B', value: 9000, fill: 'blue' },
  { name: 'Page C', value: 8000, fill: 'red' },
];

storiesOf('Chart', module)
  .addDecorator(withKnobs)
  .addDecorator(centered)
  .add('Chart', () => <Chart data={object('data', data)} />);
