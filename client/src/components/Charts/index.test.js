import React from 'react';
import Chart from './';
import renderer from 'react-test-renderer';

const data = [
  { name: 'Page A', value: 4000, fill: 'green' },
  { name: 'Page B', value: 9000, fill: 'blue' },
  { name: 'Page C', value: 8000, fill: 'red' },
];

it('renders correctly', () => {
  const tree = renderer.create(<Chart data={data} />).toJSON();
  expect(tree).toMatchSnapshot();
});
