import React, { PropTypes } from 'react';
import Button from '../Button';

const Choices = (
  {
    choices = [],
    onClick = i => i,
  },
) => (
  <div>
    {choices.map(({ id, text, count }) => (
      <div key={id}>
        <Button
          onClick={onClick.bind(null, id)}
          size="lg"
          className="btn-block"
        >
          {text}
        </Button>
      </div>
    ))}
  </div>
);

Choices.propTypes = {
  choices: PropTypes.array,
  onClick: PropTypes.func,
};

export default Choices;
