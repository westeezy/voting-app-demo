import React from 'react';
import Choices from './';
import renderer from 'react-test-renderer';

const data = [
  { id: 1, text: 'a1', count: 0 },
  { id: 2, text: 'a2', count: 1 },
  { id: 3, text: 'a3', count: 2 },
  { id: 4, text: 'a4', count: 3 },
];

it('renders correctly', () => {
  const tree = renderer
    .create(<Choices choices={data} onClick={i => console.log(i)} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
