import React from 'react';
import Error from './';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer.create(<Error />).toJSON();
  expect(tree).toMatchSnapshot();
});
