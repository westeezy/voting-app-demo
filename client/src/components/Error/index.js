import React, { PropTypes } from 'react';

const Error = (
  {
    heading = 'Oh snap!',
    message = 'Change a few things up and try submitting again.',
  },
) => (
  <div className="alert alert-danger" role="alert">
    <strong>{heading}</strong>&nbsp;
    <span>{message}</span>
  </div>
);

Error.propTypes = {
  heading: PropTypes.string,
  message: PropTypes.string,
};

export default Error;
