import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs, text } from '@kadira/storybook-addon-knobs';
import centered from '@kadira/react-storybook-decorator-centered';

import Error from './';

storiesOf('Error', module)
  .addDecorator(withKnobs)
  .addDecorator(centered)
  .add('Error', () => (
    <Error message={text('message', 'OMG THERE WAS AN EROROROROR')} />
  ));
