import React from 'react';
import PollList from './';
import renderer from 'react-test-renderer';

const data = [
  { question: 'q1', id: 1 },
  { question: 'q2', id: 2 },
  { question: 'q3', id: 3 },
];

it('renders correctly', () => {
  const tree = renderer.create(<PollList polls={data} />).toJSON();
  expect(tree).toMatchSnapshot();
});
