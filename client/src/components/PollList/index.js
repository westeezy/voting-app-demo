import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const PollList = (
  {
    polls = [],
  },
) => (
  <ul className="list-group">
    {polls.map(poll => (
      <li className="list-group-item" key={poll.id}>
        <Link to={`/polls/${poll.id}`}>{poll.question}</Link>
      </li>
    ))}
  </ul>
);

PollList.propTypes = {
  polls: PropTypes.array,
};

export default PollList;
