import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs, text } from '@kadira/storybook-addon-knobs';
import centered from '@kadira/react-storybook-decorator-centered';

import Spinner from './';

storiesOf('Spinner', module)
  .addDecorator(withKnobs)
  .addDecorator(centered)
  .add('Spinner', () => (
    <div>
      <Spinner
        color={text('color', '#D43B11')}
        height={text('height', '20vmin')}
      />
    </div>
  ));
