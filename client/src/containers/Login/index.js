import React, { Component, PropTypes } from 'react';
import Loader from 'react-loader';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import { logIn } from '../../modules/auth';
import Button from '../../components/Button';
import Error from '../../components/Error';

export class Login extends Component {
  static defaultProps = {
    error: false,
    loading: false,
  };

  static propTypes = {
    error: PropTypes.bool,
    loading: PropTypes.bool,
  };

  state = {
    username: '',
    password: '',
  };

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { username, password } = this.state;
    this.props.logIn(username, password);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  render() {
    return (
      <div className="container">
        <div className="content-container text-center">
          <h2>Welcome!</h2>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <input
                name="username"
                onChange={this.handleChange}
                value={this.state.username}
                type="text"
                className="form-control"
                placeholder="Enter username"
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                name="password"
                onChange={this.handleChange}
                value={this.state.password}
                type="password"
                className="form-control"
              />
            </div>
            {this.props.error ? <Error /> : null}
            <Loader loaded={!this.props.loading}>
              <Button type="submit" className="btn btn-primary">Submit</Button>
            </Loader>
          </form>
          <h4>
            New users register <Link to="register">here</Link>
          </h4>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
  loading: !auth.loginLoaded,
  error: auth.loginError,
});

const mapDispatchToProps = {
  logIn,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
