import React from 'react';
import { shallow } from 'enzyme';
import { Login } from './';

describe('containers', () => {
  describe('Login', () => {
    it('should render self', () => {
      const wrapper = shallow(<Login />);
      expect(wrapper.find('.container').length).toEqual(1);
    });
  });
});
