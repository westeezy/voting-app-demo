import React from 'react';
import { shallow } from 'enzyme';
import { Profile } from './';

const data = {
  loaded: true,
  user: {
    name: 'test',
    polls: [{ id: 1, question: 'test1' }, { id: 2, question: 'test2' }],
  },
};

describe('containers', () => {
  describe('Poll', () => {
    it('should render self', () => {
      const wrapper = shallow(<Profile profile={data} />);
      expect(wrapper.find('.profile').length).toEqual(1);
    });
  });
});
