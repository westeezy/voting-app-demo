import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Loader from 'react-loader';

import { fetchProfile } from '../../modules/profile';
import PollList from '../../components/PollList';

export class Profile extends Component {
  static propTypes = {
    loaded: PropTypes.bool,
    user: PropTypes.object,
  };

  static defaultProps = {
    loaded: false,
    polls: [],
  };

  componentDidMount() {
    // always freshly pull for new polls
    // TODO: cache these better once data is normalized
    this.props.fetch();
  }

  render() {
    const { profile } = this.props;
    return (
      <div className="profile">
        <h4 className="page-header text-center">
          Polls by {profile.user.username}:
        </h4>
        <Loader loaded={profile.loaded}>
          {profile.user.polls.length
            ? <PollList polls={profile.user.polls} />
            : <div>You should make some polls...</div>}
        </Loader>
      </div>
    );
  }
}

const mapStateToProps = ({ profile }) => ({
  profile,
});

const mapDispatchToProps = {
  fetch: fetchProfile,
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
