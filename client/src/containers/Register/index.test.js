import React from 'react';
import { shallow } from 'enzyme';
import { Register } from './';

describe('containers', () => {
  describe('Login', () => {
    it('should render self', () => {
      const wrapper = shallow(<Register />);
      expect(wrapper.find('.container').length).toEqual(1);
    });
  });
});
