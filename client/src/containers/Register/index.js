import React, { Component, PropTypes } from 'react';
import Loader from 'react-loader';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { register } from '../../modules/auth';

import Button from '../../components/Button';
import Error from '../../components/Error';

export class Register extends Component {
  static defaultProps = {
    error: false,
    loading: false,
  };

  static propTypes = {
    error: PropTypes.bool,
    loading: PropTypes.bool,
  };

  state = {
    username: '',
    password: '',
    verifyPassword: '',
  };

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { username, password, verifyPassword } = this.state;
    this.props.register(username, password, verifyPassword);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  render() {
    return (
      <div className="container">
        <div className="content-container text-center">
          <h2>Registration</h2>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <input
                name="username"
                onChange={this.handleChange}
                value={this.state.username}
                type="text"
                className="form-control"
                placeholder="Enter username"
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                name="password"
                onChange={this.handleChange}
                value={this.state.password}
                type="password"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label htmlFor="verifyPassword">Verify Password</label>
              <input
                name="verifyPassword"
                onChange={this.handleChange}
                value={this.state.verifyPassword}
                type="password"
                className="form-control"
              />
            </div>
            {this.props.error ? <Error /> : null}
            <Loader loaded={!this.props.loading}>
              <Button type="submit">Submit</Button>
            </Loader>
          </form>
          <small>
            <Link to="login">Take me back!</Link>
          </small>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
  error: auth.registrationError,
  loading: !auth.registrationLoaded,
});

const mapDispatchToProps = {
  register,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
