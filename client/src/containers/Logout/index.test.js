import React from 'react';
import { shallow } from 'enzyme';
import { Logout } from './';

describe('containers', () => {
  describe('Logout', () => {
    it('should render self', () => {
      const wrapper = shallow(<Logout />);
      // it should redirect so no container
      expect(wrapper.find('.container').length).toEqual(0);
    });
  });
});
