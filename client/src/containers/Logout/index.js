import { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { logOut } from '../../modules/auth';

// just log the user out and don't render anything
export class Logout extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
  };

  static defaultProps = {
    dispatch: i => i,
  };

  componentDidMount() {
    this.props.dispatch(logOut());
  }

  render() {
    return null;
  }
}

export default connect()(Logout);
