import React from 'react';
import { shallow } from 'enzyme';
import { Poll } from './';

describe('containers', () => {
  describe('Poll', () => {
    it('should render self', () => {
      const wrapper = shallow(<Poll loaded={true} />);
      expect(wrapper.find('.poll-index').length).toEqual(1);
    });
  });
});
