import React, { Component } from 'react';
import { connect } from 'react-redux';
import Back from '../../components/Back';
import { createPoll } from '../../modules/polls/actions';

export class CreatePoll extends Component {
  state = {
    question: '',
    option1: '',
    option2: '',
  };

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
  }

  handleCreate(e) {
    e.preventDefault();
    const { question, option1, option2 } = this.state;
    this.props.create(question, option1, option2);
  }

  // TODO: create a nice form reducer that can be used
  // as an HOC for on demand forms.
  // Also beef up validation + error messaging
  handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  render() {
    return (
      <div>
        <Back />
        <form onSubmit={this.handleCreate}>
          <div className="form-group">
            <label htmlFor="question">Question</label>
            <input
              name="question"
              onChange={this.handleChange}
              value={this.state.question}
              type="text"
              className="form-control"
              placeholder="Enter question text"
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="option1">First Option</label>
            <input
              name="option1"
              onChange={this.handleChange}
              value={this.state.option1}
              type="text"
              className="form-control"
              placeholder="Enter answer text"
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="answer">Second Option</label>
            <input
              name="option2"
              onChange={this.handleChange}
              value={this.state.option2}
              type="text"
              className="form-control"
              placeholder="Enter answer text"
              required
            />
          </div>
          <button type="submit" className="btn btn-primary">Submit</button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = {
  create: createPoll,
};

export default connect(() => ({}), mapDispatchToProps)(CreatePoll);
