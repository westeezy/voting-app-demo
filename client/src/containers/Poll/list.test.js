import React from 'react';
import { shallow } from 'enzyme';
import { Polls } from './list';

const polls = {
  polls: [{ id: 1, question: 'test1' }, { id: 2, question: 'test2' }],
};

describe('containers', () => {
  describe('Polls', () => {
    it('should render self', () => {
      const wrapper = shallow(<Polls polls={polls} loaded={true} />);
      expect(wrapper.find('.polls-list').length).toEqual(1);
    });
  });
});
