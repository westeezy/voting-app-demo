import React, { Component, PropTypes } from "react";
import { connect } from "react-redux";
import Loader from "react-loader";
import Back from "../../components/Back";
import Chart from "../../components/Charts";
import Choices from "../../components/Choices";
import { fetchPoll, vote } from "../../modules/polls/actions";

export class Poll extends Component {
  static propTypes = {
    loaded: PropTypes.bool,
    poll: PropTypes.object
  };

  static defaultProps = {
    loaded: false,
    poll: { choices: [] }
  };

  constructor(props) {
    super(props);
    this.handleVote = this.handleVote.bind(this);
  }

  componentDidMount() {
    if (!this.props.poll.loaded) {
      this.props.fetch(this.props.params.id);
    }
  }

  handleVote(id) {
    this.props.vote({
      pollId: parseInt(this.props.params.id, 10),
      choiceId: id
    });
  }

  render() {
    const { graphData, poll } = this.props;

    return (
      <div className="poll-index">
        <Back />
        <div className="text-center">
          <h4>{poll.question}</h4>
          <Loader loaded={poll.loaded}>
            {poll.voted
              ? <Chart data={graphData} />
              : <Choices choices={poll.choices} onClick={this.handleVote} />}
          </Loader>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ polls }, props) => {
  // TODO: This can be cleaned up and used in a memoized selector
  // especially if we get more data and use multiple graphs
  const pollId = parseInt(props.params.id, 10);
  const poll = polls.polls.find(p => p.id === pollId);
  const choices = poll ? (poll.choices || []) : [];

  const graphData = choices.map(choice => ({
    name: choice.text,
    value: choice.count
  }));

  return {
    poll,
    graphData
  };
};

const mapDispatchToProps = {
  fetch: fetchPoll,
  vote
};

export default connect(mapStateToProps, mapDispatchToProps)(Poll);
