import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Loader from 'react-loader';

import Button from '../../components/Button';
import PollList from '../../components/PollList';
import { fetchPolls } from '../../modules/polls/actions';

export class Polls extends Component {
  static propTypes = {
    loaded: PropTypes.bool,
    polls: PropTypes.object,
  };

  static defaultProps = {
    loaded: false,
    polls: [],
  };

  componentDidMount() {
    if (!this.props.polls.loaded) {
      this.props.fetch();
    }
  }

  render() {
    const { loaded, polls } = this.props.polls;
    return (
      <div className="polls-list">
        <div className="page-header text-center">
          <h4>Active Polls:</h4>
          <Link to="polls/new">
            <Button size="sm">Create New</Button>
          </Link>
        </div>
        <Loader loaded={loaded}>
          <PollList polls={polls} />
        </Loader>
      </div>
    );
  }
}

const mapStateToProps = ({ polls }) => ({
  polls,
});

const mapDispatchToProps = {
  fetch: fetchPolls,
};

export default connect(mapStateToProps, mapDispatchToProps)(Polls);
