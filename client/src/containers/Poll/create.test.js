import React from 'react';
import { shallow } from 'enzyme';
import { CreatePoll } from './create';

describe('containers', () => {
  describe('CreatePoll', () => {
    it('should render self', () => {
      const wrapper = shallow(<CreatePoll />);
      expect(wrapper.find('.form-group').length).toEqual(3);
    });
  });
});
