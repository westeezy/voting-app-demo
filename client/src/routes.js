import React from 'react';
import { Route, IndexRoute } from 'react-router';

import AppLayout from './layouts';
import Login from './containers/Login';
import Logout from './containers/Logout';
import Register from './containers/Register';
import Profile from './containers/Profile';
import Poll from './containers/Poll';
import CreatePoll from './containers/Poll/create';
import Polls from './containers/Poll/list';

import { isAuthenticated } from './utils/auth';

const NotFound = () => <h1>Not Found!</h1>;

function requireAuth(nextState, replace) {
  if (!isAuthenticated()) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname },
    });
  }
}

export default [
  <Route path="login" component={Login} />,
  <Route path="logout" component={Logout} />,
  <Route path="register" component={Register} />,
  (
    <Route path="/" component={AppLayout} onEnter={requireAuth}>
      <Route path="polls">
        <IndexRoute component={Polls} />
        <Route path="new" component={CreatePoll} />
        <Route path=":id" component={Poll} />
      </Route>
      <Route path="profile" component={Profile} />
      <Route path="*" component={NotFound} />
    </Route>
  ),
];
