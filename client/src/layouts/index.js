import React, { Component } from 'react';
import Header from '../components/Header';

export default class App extends Component {
  static defaultProps = {
    location: {
      pathname: '',
    },
  };

  constructor(props) {
    super(props);
    this.isActive = this.isActive.bind(this);
  }

  isActive(path) {
    return this.props.location.pathname.includes(path);
  }

  render() {
    return (
      <div className="container">
        <div className="content-container">
          <div className="text-center">
            <img
              alt="cats voting"
              className="voting-banner"
              width="250"
              src="/voting-cats.jpg"
            />
          </div>
          <Header isActive={this.isActive} />
          {this.props.children}
        </div>
      </div>
    );
  }
}
