import React from 'react';
import App from './';

import { shallow } from 'enzyme';

it('should render', () => {
  const wrapper = shallow(<App>test</App>);
  expect(wrapper.find('.container').length).toBe(1);
});
