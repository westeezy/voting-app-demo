import 'isomorphic-fetch';
import './App.css';

import React from 'react';
import ReactDOM from 'react-dom';

// routes
import { browserHistory } from 'react-router';
import routes from './routes';

// root container which sets up router + stores
import Root from './containers/root';

// store
import store from './modules';

ReactDOM.render(
  <Root history={browserHistory} routes={routes} store={store} />,
  document.getElementById('root'),
);
