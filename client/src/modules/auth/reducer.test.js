import reducer from './';
import {
  LOGIN_FETCH,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT,
  REGISTRATION_FETCH,
  REGISTRATION_SUCCESS,
  REGISTRATION_ERROR,
} from './';

describe('auth reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      loginError: false,
      loginLoaded: true,
      registrationError: true,
      registrationLoaded: true,
      token: '',
      user: {},
    });
  });

  it('should handle FETCH_LOGIN', () => {
    expect(
      reducer({}, {
        type: LOGIN_FETCH,
      }),
    ).toEqual({
      loginError: false,
      loginLoaded: false,
      registrationError: false,
      registrationLoaded: true,
      token: '',
      user: {},
    });
  });

  it('should handle LOGIN_SUCCESS', () => {
    expect(
      reducer({}, {
        type: LOGIN_SUCCESS,
        payload: { token: '123', user: { id: '456' } },
      }),
    ).toEqual({
      loginError: false,
      loginLoaded: true,
      registrationError: false,
      registrationLoaded: true,
      token: '123',
      user: { id: '456' },
    });
  });

  it('should handle LOGIN_ERROR', () => {
    expect(
      reducer({}, {
        type: LOGIN_ERROR,
        error: 'err',
      }),
    ).toEqual({ loginError: true, loginLoaded: true, token: '', user: {} });
  });

  it('should handle LOGOUT', () => {
    expect(
      reducer({}, {
        type: LOGOUT,
      }),
    ).toEqual({});
  });

  it('should handle REGISTRATION_FETCH', () => {
    expect(
      reducer({}, {
        type: REGISTRATION_FETCH,
      }),
    ).toEqual({ registrationLoaded: false });
  });

  it('should handle REGISTRATION_SUCCESS', () => {
    expect(
      reducer({}, {
        type: REGISTRATION_SUCCESS,
      }),
    ).toEqual({
      loginError: false,
      loginLoaded: true,
      registrationError: false,
      registrationLoaded: true,
      token: '',
      user: {},
    });
  });

  it('should handle REGISTRATION_ERROR', () => {
    expect(
      reducer({}, {
        type: REGISTRATION_ERROR,
      }),
    ).toEqual({ registrationError: true, registrationLoaded: true });
  });
});
