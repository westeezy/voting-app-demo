import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import { logIn, register } from './';
import {
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT,
  REGISTRATION_SUCCESS,
  REGISTRATION_ERROR,
} from './';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('auth actions', () => {
  afterEach(() => {
    fetchMock.restore();
  });

  it('should login successfully', () => {
    const store = mockStore({});
    fetchMock.post('*', { token: '123', user: {} });
    const expectedActions = [
      { type: 'LOGIN_FETCH' },
      { payload: '{"token":"123","user":{}}', type: 'LOGIN_SUCCESS' },
    ];
    return store.dispatch(logIn('test', 'test')).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should register successfully', () => {
    const store = mockStore({});
    fetchMock.post('*', { token: '123', user: {} });

    const expectedActions = [
      { type: 'REGISTRATION_FETCH' },
      { payload: '{"token":"123","user":{}}', type: 'REGISTRATION_SUCCESS' },
    ];

    return store.dispatch(register('test', 'test', 'test')).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should fail login with bad network', () => {
    const store = mockStore({});
    fetchMock.post('*', 500);
    return store.dispatch(logIn('test', 'test')).then(() => {
      expect(store.getActions()[1].error).toEqual(true);
    });
  });

  it('should fail register with bad network ', () => {
    const store = mockStore({});
    fetchMock.post('*', 500);
    return store.dispatch(register('test', 'test', 'test')).then(() => {
      expect(store.getActions()[1].error).toEqual(true);
    });
  });

  it('should NOT register with unmatched passwords', () => {
    const store = mockStore({});
    const expectedActions = [
      { type: 'REGISTRATION_FETCH' },
      { payload: 'Passwords must match', type: 'REGISTRATION_ERROR' },
    ];

    store.dispatch(register('test', 'test', 'not'));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
