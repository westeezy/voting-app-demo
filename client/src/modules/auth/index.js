import { createAction, handleActions } from 'redux-actions';
import { browserHistory } from 'react-router';
import * as api from '../../api';

// ------------------------------------
// Constants
// ------------------------------------
export const LOGIN_FETCH = 'LOGIN_FETCH';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

export const LOGOUT = 'LOGOUT';

export const REGISTRATION_FETCH = 'REGISTRATION_FETCH';
export const REGISTRATION_SUCCESS = 'REGISTRATION_SUCCESS';
export const REGISTRATION_ERROR = 'REGISTRATION_ERROR';

// ------------------------------------
// Actions
// ------------------------------------
const fetchLogin = createAction(LOGIN_FETCH);
const successfulLogin = createAction(LOGIN_SUCCESS);
const erroredLogin = createAction(LOGIN_ERROR);

export const logIn = (username, password) => {
  return (dispatch, getState) => {
    dispatch(fetchLogin());
    return api
      .logIn(username, password)
      .then(response => {
        localStorage.setItem('token', response.token);
        dispatch(successfulLogin(response));
        browserHistory.push('/polls');
      })
      .catch(err => dispatch(erroredLogin(err)));
  };
};

const logout = createAction(LOGOUT);
export const logOut = () => {
  return (dispatch, getState) => {
    localStorage.removeItem('token');
    // might eventually have something async to server
    // so leaving thunks in place for now.
    dispatch(logout());
    browserHistory.push('login');
  };
};

const fetchRegistration = createAction(REGISTRATION_FETCH);
const successfulRegistration = createAction(REGISTRATION_SUCCESS);
const erroredRegistration = createAction(REGISTRATION_ERROR);

export const register = (username, password, verifyPassword) => {
  return (dispatch, getState) => {
    dispatch(fetchRegistration());
    if (!password || password !== verifyPassword) {
      return dispatch(erroredRegistration('Passwords must match'));
    } else {
      return api
        .register(username, password)
        .then(response => {
          dispatch(successfulRegistration(response));
          browserHistory.push('/login');
        })
        .catch(err => dispatch(erroredRegistration(err)));
    }
  };
};

export const actions = {
  logIn,
  logOut,
  register,
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  loginError: false,
  loginLoaded: true,
  registrationLoaded: true,
  registrationError: false,
  token: '',
  user: {},
};

export default handleActions(
  {
    [LOGIN_FETCH]: (state, action) => ({
      ...initialState, // reset state
      loginLoaded: false,
    }),
    [LOGIN_SUCCESS]: (state, action) => ({
      ...initialState,
      ...action.payload,
      loginLoaded: true,
    }),
    [LOGIN_ERROR]: (state, action) => ({
      ...state,
      loginError: true,
      loginLoaded: true,
      token: '',
      user: {},
    }),
    [REGISTRATION_FETCH]: (state, action) => ({
      ...state, // reset state just in case
      registrationLoaded: false,
    }),
    [REGISTRATION_SUCCESS]: (state, action) => ({
      ...initialState,
      registrationLoaded: true,
    }),
    [REGISTRATION_ERROR]: (state, action) => ({
      ...state,
      registrationError: true,
      registrationLoaded: true,
    }),
  },
  initialState,
);
