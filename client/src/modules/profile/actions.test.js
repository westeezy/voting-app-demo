import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import { fetchProfile } from './';
import { FETCH_PROFILE_SUCCESS, FETCH_PROFILE_ERROR } from './';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('auth actions', () => {
  afterEach(() => {
    fetchMock.restore();
  });

  it('should login successfully', () => {
    const store = mockStore({});
    fetchMock.get('*', { username: 'test', polls: [] });
    const expectedActions = [
      {
        payload: '{"username":"test","polls":[]}',
        type: 'FETCH_PROFILE_SUCCESS',
      },
    ];
    return store.dispatch(fetchProfile()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should fail login with bad network', () => {
    const store = mockStore({});
    fetchMock.get('*', 500);
    return store.dispatch(fetchProfile()).then(() => {
      expect(store.getActions()[0].error).toEqual(true);
    });
  });
});
