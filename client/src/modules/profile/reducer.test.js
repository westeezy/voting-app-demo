import reducer from './';
import { FETCH_PROFILE_SUCCESS, FETCH_PROFILE_ERROR } from './';

describe('profile reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      error: true,
      loaded: true,
      user: undefined,
    });
  });

  it('should handle FETCH_PROFILE_SUCCESS', () => {
    expect(
      reducer({}, {
        type: FETCH_PROFILE_SUCCESS,
        payload: { name: 'test', polls: [] },
      }),
    ).toEqual({ loaded: true, user: { name: 'test', polls: [] } });
  });

  it('should handle FETCH_PROFILE_ERROR', () => {
    expect(
      reducer({}, {
        type: FETCH_PROFILE_ERROR,
        error: 'error',
        payload: {},
      }),
    ).toEqual({ error: true, loaded: true });
  });
});
