import { createAction, handleActions } from 'redux-actions';
import * as api from '../../api';

// ------------------------------------
// Constants
// ------------------------------------
export const FETCH_PROFILE_SUCCESS = 'FETCH_PROFILE_SUCCESS';
export const FETCH_PROFILE_ERROR = 'FETCH_PROFILE_ERROR';

// ------------------------------------
// Actions
// ------------------------------------

const fetchSuccess = createAction(FETCH_PROFILE_SUCCESS);
const fetchError = createAction(FETCH_PROFILE_ERROR);

export const fetchProfile = () => {
  return (dispatch, getState) => {
    return api
      .getProfile()
      .then(response => dispatch(fetchSuccess(response)))
      .catch(err => dispatch(fetchError(err)));
  };
};

export const actions = {
  fetchProfile,
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  error: false,
  loaded: false,
  user: {
    polls: [],
  },
};

export default handleActions(
  {
    [FETCH_PROFILE_SUCCESS]: (state, action) => ({
      ...state,
      user: action.payload,
      loaded: true,
    }),
    [FETCH_PROFILE_ERROR]: (state, action) => ({
      ...state,
      error: true,
      loaded: true,
    }),
  },
  initialState,
);
