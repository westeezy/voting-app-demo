import { createStore, applyMiddleware, combineReducers } from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';

// reducers
import polls from './polls';
import profile from './profile';
import auth, { LOGOUT } from './auth';

const logger = createLogger();

const appReducer = combineReducers({
  auth,
  polls,
  profile,
});

const rootReducer = (state, action) => {
  // restore all reducers on logout
  if (action.type === LOGOUT) {
    state = undefined;
  }

  return appReducer(state, action);
};

const store = createStore(rootReducer, applyMiddleware(thunk, logger));

export default store;
