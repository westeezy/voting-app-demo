import { createAction } from 'redux-actions';
import { browserHistory } from 'react-router';
import * as api from '../../api';
import {
  POLLS_FETCH,
  POLLS_FETCH_SUCCESS,
  POLLS_FETCH_ERROR,
  POLL_VOTE,
  POLL_FETCH,
  POLL_FETCH_SUCCESS,
  POLL_FETCH_ERROR,
} from './';

const loadingPolls = createAction(POLLS_FETCH);
const fetchedPolls = createAction(POLLS_FETCH_SUCCESS);
const erroredPolls = createAction(POLLS_FETCH_ERROR);

export const fetchPolls = () => {
  return (dispatch, getState) => {
    dispatch(loadingPolls());
    api
      .getPolls()
      .then(polls => dispatch(fetchedPolls(polls)))
      .catch(error => dispatch(erroredPolls(error)));
  };
};

const loadingPoll = createAction(POLL_FETCH);
const fetchedPoll = createAction(POLL_FETCH_SUCCESS);
const erroredPoll = createAction(POLL_FETCH_ERROR);

export const fetchPoll = id => {
  return (dispatch, getState) => {
    dispatch(loadingPoll());
    api
      .getPollById(id)
      .then(poll => dispatch(fetchedPoll(poll)))
      .catch(error => dispatch(erroredPoll(error)));
  };
};

const votedInPoll = createAction(POLL_VOTE);

export const vote = payload => {
  return (dispatch, getState) => {
    // optimistically vote
    api.voteOnPoll(payload.choiceId);
    dispatch(votedInPoll(payload));
  };
};

export const createPoll = (question, ...options) => {
  return (dispatch, getState) => {
    dispatch(loadingPolls());
    api
      .createPoll(question, ...options)
      .then(() => browserHistory.push('/polls'))
      .catch(err => erroredPoll(err));
    // TODO: proper handling of above
  };
};

export default {
  fetchPolls,
  fetchPoll,
  vote,
};
