import reducer from './';
import {
  POLLS_FETCH,
  POLLS_FETCH_SUCCESS,
  POLLS_FETCH_ERROR,
  POLL_VOTE,
  POLL_FETCH_SUCCESS,
  POLL_FETCH_ERROR,
} from './';

const loadedReducerState = {
  error: false,
  loaded: true,
  polls: [
    {
      id: 1,
      question: 'q1',
      choices: [
        { id: 1, text: 'test1', count: 10 },
        { id: 2, text: 'test2', count: 3 },
      ],
    },
    {
      id: 2,
      question: 'q2',
      choices: [
        { id: 3, text: 'test3', count: 4 },
        { id: 4, text: 'test4', count: 5 },
      ],
    },
  ],
};

describe('polls reducer', () => {
  it('should handle POLLS_FETCH', () => {
    expect(
      reducer({}, {
        type: POLLS_FETCH,
        payload: undefined,
      }),
    ).toEqual({ loaded: false });
  });

  it('should handle POLLS_FETCH_SUCCESS', () => {
    expect(
      reducer({}, {
        type: POLLS_FETCH_SUCCESS,
        payload: [{ question: 'test' }, { question: 'test2' }],
      }),
    ).toEqual({
      loaded: true,
      polls: [
        { loaded: false, question: 'test' },
        { loaded: false, question: 'test2' },
      ],
    });
  });

  it('should handle POLLS_FETCH_ERROR', () => {
    expect(
      reducer({}, {
        type: POLLS_FETCH_ERROR,
        error: 'error',
        payload: {},
      }),
    ).toEqual({
      error: true,
    });
  });

  it('should handle POLL_VOTE', () => {
    expect(
      reducer(loadedReducerState, {
        type: POLL_VOTE,
        payload: { pollId: 2, choiceId: 3 },
      }),
    ).toEqual({
      error: false,
      loaded: true,
      polls: [
        {
          choices: [
            { count: 10, id: 1, text: 'test1' },
            { count: 3, id: 2, text: 'test2' },
          ],
          id: 1,
          question: 'q1',
        },
        {
          choices: [
            { count: 5, id: 3, text: 'test3' },
            { count: 5, id: 4, text: 'test4' },
          ],
          id: 2,
          question: 'q2',
          voted: true,
        },
      ],
    });
  });

  it('should handle POLL_FETCH_SUCCESS with new poll', () => {
    expect(
      reducer(loadedReducerState, {
        type: POLL_FETCH_SUCCESS,
        payload: {
          id: 3,
          question: 'q3',
          choices: [{ id: 5, text: 'test5' }, { id: 6, text: 'test6' }],
        },
      }),
    ).toEqual({
      error: false,
      loaded: true,
      loadedCurrent: true,
      polls: [
        {
          choices: [
            { count: 10, id: 1, text: 'test1' },
            { count: 3, id: 2, text: 'test2' },
          ],
          id: 1,
          question: 'q1',
        },
        {
          choices: [
            { count: 5, id: 3, text: 'test3' },
            { count: 5, id: 4, text: 'test4' },
          ],
          id: 2,
          question: 'q2',
        },
      ],
    });
  });

  it('should handle POLL_FETCH_SUCCESS with existing poll', () => {
    expect(
      reducer(loadedReducerState, {
        type: POLL_FETCH_SUCCESS,
        payload: {
          id: 2,
          question: 'q3',
          choices: [{ id: 5, text: 'test5' }, { id: 6, text: 'test6' }],
        },
      }),
    ).toEqual({
      error: false,
      loaded: true,
      loadedCurrent: true,
      polls: [
        {
          choices: [
            { count: 10, id: 1, text: 'test1' },
            { count: 3, id: 2, text: 'test2' },
          ],
          id: 1,
          question: 'q1',
        },
        {
          choices: [{ id: 5, text: 'test5' }, { id: 6, text: 'test6' }],
          id: 2,
          loaded: true,
          question: 'q3',
        },
      ],
    });
  });

  it('should handle POLL_FETCH_ERROR', () => {
    expect(
      reducer(loadedReducerState, {
        type: POLL_FETCH_ERROR,
        error: 'error',
        payload: {},
      }),
    ).toEqual({
      error: false,
      errorCurrent: true,
      loaded: true,
      polls: [
        {
          choices: [
            { count: 10, id: 1, text: 'test1' },
            { count: 3, id: 2, text: 'test2' },
          ],
          id: 1,
          question: 'q1',
        },
        {
          choices: [
            { count: 5, id: 3, text: 'test3' },
            { count: 5, id: 4, text: 'test4' },
          ],
          id: 2,
          question: 'q2',
        },
      ],
    });
  });
});
