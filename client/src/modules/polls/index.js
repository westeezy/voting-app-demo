import { handleActions } from 'redux-actions';

// ------------------------------------
// Constants
// ------------------------------------
export const POLLS_FETCH = 'POLLS_FETCH';
export const POLLS_FETCH_SUCCESS = 'POLLS_FETCH_SUCCESS';
export const POLLS_FETCH_ERROR = 'POLLS_FETCH_ERROR';

export const POLL_VOTE = 'POLL_VOTE';
export const POLL_FETCH = 'POLL_FETCH';
export const POLL_FETCH_SUCCESS = 'POLL_FETCH_SUCCESS';
export const POLL_FETCH_ERROR = 'POLL_FETCH_ERROR';

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  error: false,
  loaded: false,
  polls: [],
};

export default handleActions(
  {
    [POLL_VOTE]: (state, action) => {
      const { pollId, choiceId } = action.payload;
      const polls = state.polls.map(poll => {
        if (poll.id === pollId) {
          const updateChoices = poll.choices.map(choice => {
            return choice.id === choiceId
              ? { ...choice, count: ++choice.count }
              : choice;
          });
          return {
            ...poll,
            voted: true,
            choices: updateChoices,
          };
        }
        return poll;
      });

      return {
        ...state,
        polls,
      };
    },
    [POLLS_FETCH]: (state, action) => {
      return {
        ...state,
        loaded: false,
      };
    },
    [POLLS_FETCH_SUCCESS]: (state, action) => {
      return {
        ...state,
        loaded: true,
        polls: action.payload.map(poll => ({ ...poll, loaded: false })),
      };
    },
    [POLLS_FETCH_ERROR]: (state, action) => {
      return {
        ...state,
        error: true,
      };
    },
    [POLL_FETCH_SUCCESS]: (state, action) => {
      let polls = state.polls;
      const newPoll = { ...action.payload, loaded: true };

      if (polls.length === 0) {
        polls = [newPoll];
      } else {
        polls = polls.map(p => {
          return p.id === newPoll.id ? newPoll : p;
        });
      }

      return {
        ...state,
        polls,
        loadedCurrent: true,
      };
    },
    [POLL_FETCH_ERROR]: (state, action) => {
      return {
        ...state,
        errorCurrent: true,
      };
    },
  },
  initialState,
);
