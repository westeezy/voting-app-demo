import isObject from 'lodash.isobject';

import { getToken } from '../utils/auth';

const API_BASE = process.env.REACT_APP_API_BASE || 'http://localhost:3001';

const enhancedFetch = (url, config) => {
  if (config && config.method === 'POST' && isObject(config.body)) {
    config.headers = config.headers || {};
    config.body = JSON.stringify(config.body);
    config.headers['Content-Type'] = 'application/json';
  }

  return fetch(url, config).then(response => {
    const type = response.headers.get('content-type');
    if (!response.ok) {
      const err = new Error(response.statusText);
      err.code = response.status;
      throw err;
    }
    return /application\/json/.test(type) ? response.json() : response.text();
  });
};

const fetchWithToken = (url, config = {}) => {
  const token = getToken();
  const baseHeaders = {
    Authorization: token && `Bearer ${token}`,
    Accept: 'application/json',
  };

  const updatedConfig = {
    ...config,
    headers: {
      ...(config.headers || {}),
      ...baseHeaders,
    },
  };

  return enhancedFetch(url, updatedConfig);
};

export const getPolls = () => {
  return fetchWithToken(`${API_BASE}/poll`);
};

export const getPollById = id => {
  return fetchWithToken(`${API_BASE}/poll/${id}`);
};

export const createPoll = (question, ...options) => {
  return fetchWithToken(`${API_BASE}/poll`, {
    method: 'POST',
    body: {
      question,
      options,
    },
  });
};

export const voteOnPoll = choiceId => {
  return fetchWithToken(`${API_BASE}/vote/${choiceId}`, { method: 'PUT' });
};

export const getProfile = () => {
  return fetchWithToken(`${API_BASE}/user`);
};

export const logIn = (username, password) => {
  return enhancedFetch(`${API_BASE}/login`, {
    method: 'POST',
    body: {
      username,
      password,
    },
  });
};

export const register = (username, password) => {
  return enhancedFetch(`${API_BASE}/register`, {
    method: 'POST',
    body: {
      username,
      password,
    },
  });
};
