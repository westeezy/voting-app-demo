const server = require('pushstate-server');

console.log('Listening on :3000');

server.start({
  port: 3000,
  directory: './build',
});
