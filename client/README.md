# Voting App Client

The voting app client was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app) in order to make things quicker. In general CRA is an awesome tool anyways.

Currently deployed on a free version of now.sh at [https://voting-client-ynnsequoll.now.sh/](https://voting-client-ynnsequoll.now.sh/). It may run a little on the slow slide but it's sufficient to demo it. Plus a good excuse to try that service.


## Installation

The app is built leveraging [Yarn](https://github.com/yarnpkg) so I reccomend continuing to use that. The default api path is `http://localhost:3001` but it can be modified using the `API_BASE` environment variable.

* `yarn`
* `yarn start`

If you want to start using the API server deployed on heroku you can use, but not you may have issues with HTTP -> HTTPS
* `yarn start:heroku`

To build using the now server you can also use
* `yarn build:now`

## Directory Structure
* components
	* Presentational components that are very little logic 
* containers
	* connected to redux
* modules
	* a way to container redux "things". It is nice to keep actions, constants, and reducers in one file as you are building them and split it into multiple files as neccessary.

## Storybook
[React Storybook](https://github.com/storybooks/react-storybook) is one of my favorite tools for building reusable UI components. It also makes fuzz testing easy. I prefer to slowly build up shared component libraries and ideally later split them into a shared component package on a private npm registry. This allows for consistency amongst difffernt apps and faster build out due to the shared UI base. 

To run:

* `yarn storybook`
* open `http://localhost:9001`

## Noteable Libs and Why

* Recharts
	* Quick library for simple svg charts. I typically use a D3 variant or D3 directly but this app only has one chart so I wanted something extra lightweight that I didn't have to build myself. Recharts fit the bill nicely and I enjoyed using it.
* Redux
	* I normally will not use Redux on small apps, but to demo an app that could go I wanted to use Redux to showcase some architectural decisions when using a popular state management system for extra large apps.
* React Router  
	* Pretty much defacto these days  

## Tests
Using [Jest](https://facebook.github.io/jest/) and leveraging snapshot tests for presentational components. Most things are tested but due to time constraints not everything is 100% tested. TODOs include better tests on containers (currently they just test rendering).

Test files are colocated in the filesystem via `*.test.js` extensions.

To run:

* `yarn test`

## Deployment

I have included a Dockerfile that spins up the default webpack server so not the best idea for production. I would reccomend distributing this on CloudFront. Since this app uses HTML5 PushState if you deploy on CloudFront make sure to set the error document to `index.html`. See [this](http://stackoverflow.com/a/35354677). I reccomend a CDN like Cloudfront as you can get faster serving times to folks all over the world due to CDNs behavior of copying files to edge nodes worldwide.

## Feature Enhancements

* API supports N choices but UI currently only supports 2
* API supports looking up other users by id but UI currently does not
* Sorting / Seraching Polls - requires API work as well
* Paginating or Virtual Scrolling with [react-virtualized](https://bvaughn.github.io/react-virtualized/)
* More data in profiles to make it feel more social
* Using sockets via [socket.io](http://socket.io/) or webtorrent like [GUN](http://gun.js.org/#step1) for instant change updates
* [styled-components](https://github.com/styled-components/styled-components) for a better shared component ui and styling scheme.

## Tech Debt

* Global error banner for bad requests
	* Inform the user requests failed rather than spinning
* Forms to reducers. I like creating temp reducers keyed by form name that are cleared on unmount so that you can just decorate with an HOC to bind form data easily via redux. 
* Implement refresh tokens and refresh before expiration else redirect to login  
* Normalize data in reducers
	* Mostly for Poll data
	* Currently we store it as is in the reducers and do a few nasty object merges in the reducer to keep track of the data. I would reccomend normalizing so that we can update choices and poll questions indepently of each other and stich them together in a (selector)[http://redux.js.org/docs/recipes/ComputingDerivedData.html]. I often use [reselect](https://github.com/reactjs/reselect) for that but any LRU memoize should be sufficient. 
	* I would probably leverage a sturcture like
	
		```json
		{
			polls: {
				0: {
					question: '',
					completed: false,
				},
				1: {
					...
				},
				...
			},
			choices: {
				0: {
					pollId: 0,
					text: '',
					count: 100,
				}
				1: {
					....
				},
				...
			}
		}
		```


